import React from 'react';
import './todo.css';
import Header from "./components/Header";
import FooterAction from "./components/FooterAction";
import Footer from "./components/Footer";
import List from "./components/List";

const Todo: React.FC = () => {
    return (
        <div>
            <section className="todoapp">
                <Header/>
                <section className="main">
                    <input id="toggle-all" className="toggle-all" type="checkbox"/>
                    <label htmlFor="toggle-all">Mark all as complete</label>
                    <List/>
                </section>
                <FooterAction/>
            </section>
            <Footer/>
        </div>
    );
};

export default Todo;
