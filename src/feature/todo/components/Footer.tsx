import React from 'react';

const Footer: React.FC = () => {
    return (
        <footer className="info">
            <p>Created by <a href="#valmir">Valmir Barbosa</a></p>
            <p>Part of <a href="https://github.com/tastejs/todomvc-app-template">TodoMVC</a></p>
        </footer>
    );
};

export default Footer;
